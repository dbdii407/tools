"use strict";

const events = require('events');
const lo = require('lodash');

function Inherits(sc) {
  return {
    with: function(...mixins) {
      for (let mixin of mixins) {
        if (mixin == 'EventEmitter') // Shortcut
          mixin = module.exports.EventEmitter;
        
        if (lo.hasIn(mixin, 'prototype')) {
          lo.extend(sc, mixin.prototype, {});

          if (lo.hasIn(mixin.prototype, 'constructor'))
            mixin.prototype.constructor.call(sc);
        }

        else if (lo.isObject(mixin))
          lo.extend(sc, {}, mixin);
      }

      return sc;
    }
  }
}

module.exports.inherits = (superclass) => new Inherits(superclass); // Good!

module.exports.EventEmitter = function EventEmitter() {
  let _ = new Inherits(this).with(events.EventEmitter, {
    onUntil: function(event, func) {
      let handler = func.bind({
        done: function() { this.removeListener(event, handler); }.bind(this)
      });
      
      this.on(event, handler);
    }
  });
}
